# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
* Laravel  API -> Test
* Version 1.0
* Author: Adrian Costache
### How do I get set up? ###
Install Composer (See https://getcomposer.org/doc/00-intro.md)
(for Mac OS X)
 - git clone https://Rubikdesign@bitbucket.org/Rubikdesign/test-godaddy.gitOpen  
 - cd to the document root of the test-godaddy 
- After installation, open the hidden file .env (which is located in the document root) in any text editor of your choice
Setup the db section like this:
DB_CONNECTION=mysql
DB_HOST=localhost;unix_socket=/Applications/MAMP/tmp/mysql/mysql.sock
DB_PORT=3306
DB_DATABASE={database_name}
DB_USERNAME={user_name}
DB_PASSWORD={password}

- If you have a different user and password for mysql you need to use these instead of root/root
- Open the file config/database.php and set database there too:

'mysql' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', 'localhost'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', '{database_name}'),
            'username' => env('DB_USERNAME', '{user_name}'),
            'password' => env('DB_PASSWORD', '{password}'),
            'unix_socket' => env('DB_SOCKET', '/Applications/MAMP/tmp/mysql/mysql.sock'),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
           ],

8. Copy the hidden “.htaccess” file from the “public” folder directly in the document root of your laravel.test host


- Run composer install or php composer.phar install
- Run php artisan key:generate
- Run php artisan migrate
- You should see something like this:

Migration table created successfully.
Migrating: 2018_04_13_131549_create_domains_table
Migrated:  2018_04_13_131549_create_domains_table
Migrating: 2018_04_13_131700_create_domains_dns_table
Migrated:  2018_04_13_131700_create_domains_dns_table


- Run php artisan serve



